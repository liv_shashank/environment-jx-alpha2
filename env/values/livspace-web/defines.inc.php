<?php
/*
 * 2007-2011 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2011 PrestaShop SA
 *  @version  Release: $Revision: 7046 $
 *  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

define('_PS_MODE_DEV_', false);

$currentDir = dirname(__FILE__);

if (!defined('PHP_VERSION_ID')) {
    $version = explode('.', PHP_VERSION);
    define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
}

/* Theme URLs */
define('_THEMES_DIR_', __PS_BASE_URI__ . 'themes/');
define('_THEME_DIR_', _THEMES_DIR_ . _THEME_NAME_ . '/');
define('_THEME_IMG_DIR_', _THEME_DIR_ . 'img/');
define('_THEME_CSS_DIR_', _THEME_DIR_ . 'css/');
define('_THEME_JS_DIR_', _THEME_DIR_ . 'js/');

/* Image URLs */
define('_PS_IMG_', __PS_BASE_URI__ . 'img/');
define('_PS_ADMIN_IMG_', _PS_IMG_ . 'admin/');
define('_PS_TMP_IMG_', _PS_IMG_ . 'tmp/');
define('_THEME_CAT_DIR_', _PS_IMG_ . 'c/');
define('_THEME_PROD_DIR_', _PS_IMG_ . 'p/');
define('_THEME_MANU_DIR_', _PS_IMG_ . 'm/');
define('_THEME_SCENE_DIR_', _PS_IMG_ . 'scenes/');
define('_THEME_SCENE_THUMB_DIR_', _PS_IMG_ . 'scenes/thumbs');
define('_THEME_SUP_DIR_', _PS_IMG_ . 'su/');
define('_THEME_SHIP_DIR_', _PS_IMG_ . 's/');
define('_THEME_STORE_DIR_', _PS_IMG_ . 'st/');
define('_THEME_LANG_DIR_', _PS_IMG_ . 'l/');
define('_THEME_COL_DIR_', _PS_IMG_ . 'co/');
define('_SUPP_DIR_', _PS_IMG_ . 'su/');
define('_PS_PROD_IMG_', 'img/p/');

/* Other URLs */
define('_PS_JS_DIR_', __PS_BASE_URI__ . 'js/');
define('_PS_CSS_DIR_', __PS_BASE_URI__ . 'css/');
define('_THEME_PROD_PIC_DIR_', __PS_BASE_URI__ . 'upload/');
define('_MAIL_DIR_', __PS_BASE_URI__ . 'mails/');
define('_MODULE_DIR_', __PS_BASE_URI__ . 'modules/');

/* Directories */
define('_PS_ROOT_DIR_', realpath($currentDir . '/..'));
define('_PS_CLASS_DIR_', _PS_ROOT_DIR_ . '/classes/');
define('_PS_CONTROLLER_DIR_', _PS_ROOT_DIR_ . '/controllers/');
define('_PS_TRANSLATIONS_DIR_', _PS_ROOT_DIR_ . '/translations/');
define('_PS_DOWNLOAD_DIR_', _PS_ROOT_DIR_ . '/download/');
define('_PS_MAIL_DIR_', _PS_ROOT_DIR_ . '/mails/');
define('_PS_ALL_THEMES_DIR_', _PS_ROOT_DIR_ . '/themes/');
define('_PS_THEME_DIR_', _PS_ROOT_DIR_ . '/themes/' . _THEME_NAME_ . '/');
define('_PS_ADMIN_THEME_DIR_', _PS_ROOT_DIR_ . '/admin12/themes/admin-theme/');
define('_PS_IMG_DIR_', _PS_ROOT_DIR_ . '/img/');
if (!defined('_PS_MODULE_DIR_'))
    define('_PS_MODULE_DIR_', _PS_ROOT_DIR_ . '/modules/');
define('_PS_CAT_IMG_DIR_', _PS_IMG_DIR_ . 'c/');
define('_PS_STORE_IMG_DIR_', _PS_IMG_DIR_ . 'st/');
define('_PS_PROD_IMG_DIR_', _PS_IMG_DIR_ . 'p/');
define('_PS_SCENE_IMG_DIR_', _PS_IMG_DIR_ . 'scenes/');
define('_PS_SCENE_THUMB_IMG_DIR_', _PS_IMG_DIR_ . 'scenes/thumbs/');
define('_PS_MANU_IMG_DIR_', _PS_IMG_DIR_ . 'm/');
define('_PS_SHIP_IMG_DIR_', _PS_IMG_DIR_ . 's/');
define('_PS_SUPP_IMG_DIR_', _PS_IMG_DIR_ . 'su/');
define('_PS_DESIGN_IMG_DIR_', _PS_IMG_DIR_ . 'designers/');
define('_PS_MATERIAL_IMG_DIR_', _PS_IMG_DIR_ . 'materials');
define('_PS_COL_IMG_DIR_', _PS_IMG_DIR_ . 'co/');
define('_PS_TMP_IMG_DIR_', _PS_IMG_DIR_ . 'tmp/');
define('_PS_UPLOAD_DIR_', _PS_ROOT_DIR_ . '/upload/');
define('_PS_TOOL_DIR_', _PS_ROOT_DIR_ . '/tools/');
define('_PS_GEOIP_DIR_', _PS_TOOL_DIR_ . 'geoip/');
define('_PS_SWIFT_DIR_', _PS_TOOL_DIR_ . 'swift/');
define('_PS_FPDF_PATH_', _PS_TOOL_DIR_ . 'fpdf/');
define('_PS_TAASC_PATH_', _PS_TOOL_DIR_ . 'taasc/');
define('_PS_PEAR_XML_PARSER_PATH_', _PS_TOOL_DIR_ . 'pear_xml_parser/');

/* settings php */
define('_PS_TRANS_PATTERN_', '(.*[^\\\\])');
define('_PS_MIN_TIME_GENERATE_PASSWD_', '360');
if (!defined('_PS_MAGIC_QUOTES_GPC_'))
    define('_PS_MAGIC_QUOTES_GPC_', get_magic_quotes_gpc());
if (!defined('_PS_MYSQL_REAL_ESCAPE_STRING_'))
    define('_PS_MYSQL_REAL_ESCAPE_STRING_', function_exists('mysql_real_escape_string'));

define('_CAN_LOAD_FILES_', 1);

/* Order states */
define('_PS_OS_CHEQUE_', 1);
define('_PS_OS_PAYMENT_', 2);
define('_PS_OS_PREPARATION_', 3);
define('_PS_OS_SHIPPING_', 4);
define('_PS_OS_DELIVERED_', 5);
define('_PS_OS_CANCELED_', 6);
define('_PS_OS_REFUND_', 7);
define('_PS_OS_ERROR_', 8);
define('_PS_OS_OUTOFSTOCK_', 9);
define('_PS_OS_BANKWIRE_', 10);
define('_PS_OS_PAYPAL_', 11);
define('_PS_OS_WS_PAYEMENT_', 12);
define('_PS_OS_OP_PAYEMENT_ACCEPTED', 13);
define('_PS_OS_OP_PAYEMENT_FAILED', 14);
define('_PS_OS_COD_PENDING_CONFIRMATION', 15);
define('_PS_OS_READY_TO_SHIP', 16);
define('_PS_OS_LOST_DAMAGED_IN_TRANSIT', 17);
//define('_PS_OS_WS_PAYEMENT_', 18);
define('_PS_OS_WIP_DELAYED', 19);
define('_PS_OS_EDITED', 23);

/*Payment status*/
define('_PS_PS_NOT_PAID_', 1);
define('_PS_PS_PAYMENT_WITH_CARRIER_', 2);
define('_PS_PS_PAID_', 3);

/* Tax behavior */
define('PS_PRODUCT_TAX', 0);
define('PS_STATE_TAX', 1);
define('PS_BOTH_TAX', 2);

define('_PS_PRICE_DISPLAY_PRECISION_', 2);
define('PS_TAX_EXC', 1);
define('PS_TAX_INC', 0);

define('PS_ORDER_PROCESS_STANDARD', 0);
define('PS_ORDER_PROCESS_OPC', 1);

define('PS_ROUND_UP', 0);
define('PS_ROUND_DOWN', 1);
define('PS_ROUND_HALF', 2);

/* Carrier::getCarriers() filter */
define('PS_CARRIERS_ONLY', 1);
define('CARRIERS_MODULE', 2);
define('CARRIERS_MODULE_NEED_RANGE', 3);
define('PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE', 4);
define('ALL_CARRIERS', 5);

/* SQL Replication management */
define('_PS_USE_SQL_SLAVE_', 0);

/* PS Technical configuration */
define('_PS_ADMIN_PROFILE_', 1);

/* Stock Movement */
define('_STOCK_MOVEMENT_ORDER_REASON_', 3);
define('_STOCK_MOVEMENT_MISSING_REASON_', 4);

define('_PS_DEFAULT_CUSTOMER_GROUP_', 1);

define('_PS_CACHEFS_DIRECTORY_', dirname(__FILE__) . '/../cache/cachefs/');

/* Geolocation */
define('_PS_GEOLOCATION_NO_CATALOG_', 0);
define('_PS_GEOLOCATION_NO_ORDER_', 1);

/* Media versions */
define('CSS_ALL_VERSION', 214);
define('CSS_SCREEN_VERSION', 10);
define('JS_VERSION', 35);
define('IMG_VERSION', 2);
define('HTML_VERSION',4);

/* Carriers*/
define('BLUEDART', 7);
define('ARAMEX', 6);
define('QUANTIUM', 10);
define('SPEEDPOST', 11);
define('AFL', 12);
define('SABEXPRESS', 13);

if (!defined('_PS_CACHE_ENABLED_'))
    define('_PS_CACHE_ENABLED_', 0);

define('FB_API_KEY', '638936822863005');
define('FB_SECRET', 'f6cea42acd8da8a9f0558646db2d37c1');

define('CUSTOMIZATION_IMG_HOST', 'http://imgs.livspace.com/img/');
define("FP_API_KEY", "afe65ae154702c6c0d1b061bec6996b9d5f75e14");
define("FP_API_PASSWORD", "x");
define("FP_API_EMAIL", "ramakant.sharma@livspace.com");
define("FP_USER_ID", 25000473);
define("COD_CHARGE", 0);

define("FILES_DIR", "files");

define("CMS_PROD_3DS_MODELS_HIGH_DIR", "cms_products/3ds_models_high");
define("CMS_PROD_3DS_MODELS_LOW_DIR", "cms_products/3ds_models_low");
define("CMS_PROD_RHYNO_MODELS_DIR", "cms_products/rhyno_models");
define("CMS_PROD_CAD_DRAWINGS_DIR", "cms_products/cad_drawings");
define("CMS_PROD_CAD_MODELS_EXEC_DIR", "cms_products/cad_models_executive");
define("CMS_PROD_CAD_MODELS_BLOCK_DIR", "cms_products/cad_models_block");
define("CMS_PRODUCT_RAW_IMAGES_DIR", 'cms_products/raw_images');
define("CMS_PRODUCT_DIM_IMAGES_DIR", 'cms_products/dimension_images');
define("CMS_PRODUCT_RAW_IMAGES_TWO_DIR", 'cms_products/raw_images_two');
define("CMS_PRODUCT_SPECS_DIR", 'cms_products/specs');

define("MAT_LIBRARY_DIR", "mat_libraries");

define("SKU_DEFAULT_COVER_IMAGES_DIR", "skus/default_cover_images");
define("SKU_ALT_COVER_IMAGES_DIR", "skus/alt_cover_images");
define("SKU_COVER_IMAGES_DIR", "skus/cover_images");
define("SKU_RENDER_JOB_FOLDER", "skus/render_jobs");
define("SKU_MAX_MODELS_DIR", "skus/max_models");
define("SKU_CUST_IMAGES_FOLDER", "skus/ci/s");
define("SKU_CUST_IMAGES_DATA_FOLDER", "skus/cidf");
define("SKU_INSTALLATION_GUIDE_FOLDER", "skus/installation_guide");
define("SKU_TECHNICAL_DRAWING_FOLDER", "skus/technical_drawing");

define("ROOM_DEFAULT_COVER_IMAGES_DIR", "rooms/default_cover_images");
define("ROOM_COVER_IMAGES_DIR", "rooms/cover_images");
define("ROOM_MAX_MODELS_DIR", "rooms/max_models");
define("ROOM_SPECS_DIR", "rooms/specs");
define("ROOM_RENDER_JOB_FOLDER", "rooms/render_jobs");
define("ROOM_CUST_IMAGES_FOLDER", "rooms/ci/s");
define("ROOM_UNSTITCHED_CUST_IMAGES_FOLDER", "rooms/ci/us");
define("ROOM_UNSTITCHED_CUST_IMAGES_DATA_FOLDER", "rooms/cidf/us");
define("ROOM_STITCHED_CUST_IMAGES_DATA_FOLDER", "rooms/cidf/s");

define("MATERIAL_RAW_IMAGES_DIR", 'materials/raw_images');
define("MATERIAL_SWATCH_IMAGES_DIR", 'materials/swatch_images');
define("MATERIAL_BIG_IMAGES_DIR", 'materials/big_images');
define("MATERIAL_TEXTURE_IMAGES_DIR", 'materials/texture_images');

define("SPEC_COVER_IMAGES_DIR", "specs/cover_images");
//define("PROFILE_PICTURES_DIR", "designers/profile_pictures");

define("DEMO_EMPLOYEE_ID", 14);
define("DESIGN_SERVICES_CATID", 106);
define("DESIGN_SERVICES_30MIN", 815);
define("DESIGN_SERVICES_EXTENDED", 814);

define("MOOD_BOARD_DEFAULT_COVER_IMAGES_DIR", "mood_boards/default_cover_images");

define("BUILDING_IMAGES_DIR", 'buildings/images');
define("BUILDING_BANNERS_DIR", 'buildings/banners');
define("BUILDING_FILES_DIR", 'buildings/files');
define("BUILDING_FLOOR_PLAN_DIR", 'floor_plan');
define("BUILDING_FLOOR_PLAN_ISOMETRIC_DIR", 'iso');
define("FEATURE_PLACEMENT_IMAGES_DIR", 'feature_placements/images');
define("LAYOUT_IMAGES_DIR", 'layouts/images');
define("SITE_IMAGES_DIR", 'site/images');
define("DESIGN_IMAGE_DIR", 'design/images');

define("AMENITIES_ICON_IMAGES", 'amenities/images');

define('GOSF15K', 11);
define('GOSF50K', 12);
define('GOSF100K', 13);

define('SPAM_CHECK','73ee83eff062bb41a302350ccddd7095');

define('HUBSPOT_APIKEY','c110a014-70ea-43d8-aa38-728dd8dd8dd4');
define('HUBSPOT_COMPANY_ID', 510486);
define('HUBSPOT_OWNER','arpit.khandelwal@livspace.com');

define("PUSH_SERVER_BROADCAST_API","/api/v2/broadcast");
define("PUSH_SERVER_PUSH_API","/api/v2/push");
define("PUSH_SERVER_TOKEN_HANDLER_API","/api/v2/tokens");

define("HTTP_HEADER_KEY_X_APP_NAME","X-AN-APP-Name");
define("HTTP_HEADER_KEY_X_APP_KEY","X-AN-APP-KEY");

define("ANDROID_PLATFORM_KEY","gcm");
define("IOS_PLATFORM_KEY","apns");

define('PS_CUSTOMER_DEV_EMPLOYEE_GROUP', 5);

define("COMMERCIAL_LANDING_PAGE_ID", 16);
define("COMMERCIAL_DESIGN_QUERY_URL", "/commercial-interiors/design-query");
define("COMMERCIAL_THANK_YOU_URL", "/commercial-interiors/thank-you");
define("CUSTOMER_DESIGN_QUERY_URL", "/home-interiors/design-query");
define("CUSTOMER_THANK_YOU_URL", '/home-interiors/design-query/thank-you');

