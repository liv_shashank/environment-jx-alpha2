var env = {
  BOUNCER_LOGOUT_URL: 'https://bouncer.alpha2.livspace.com/logout',
  LS_WEB_PATH: 'https://vega.alpha2.livspace.com/',
  DOMAIN_TITLE: 'LivSpace Admin',
  API_URL: 'https://vega-backend.alpha2.livspace.com/',
  BOUNCER_URL: 'https://bouncer.alpha2.livspace.com/login?next=',
  REDIRECT_URL: 'https://vega.alpha2.livspace.com/lsweb/auth?next=',
  IMAGE_UPLOAD_URL: 'https://vega-backend.alpha2.livspace.com/v1/api/medias/bulkupload/',
  ASSET_UPLOAD_URL: 'https://vega-backend.alpha2.livspace.com/v1/api/assets/bulk-save/',
  DESIGN_TOOL: 'https://vega-backend.alpha2.livspace.com/v1/api/open-design-tool/',
  SENTRY_ENV: 'alpha2',
  SENTRY_URL: 'https://c7ff5c463c7840899163f9e6669ebfa9@sentry.livspace.com/7'
}
